package App::perldebs;

use strict;
use warnings;

use Module::CPANfile;

use Moo;

=head2 run

Runs dh-make-perl to locate the Perl modules.  Which modules are to be
located is specified either on the command line or in cpanfile.

=cut

sub run {
    my ($self) = @_;

    # read perl modules as arguments or from ./cpanfile
    my @modules = @ARGV;

    unless (@ARGV) {
        my $prereqs = Module::CPANfile->load->prereq_specs;

        for my $phase (%$prereqs) {
            push @modules, keys %{ $prereqs->{$phase}->{requires} };
        }
    }

    exit 1 unless @modules;
    # locate Debian packages that include these modules
    my $cmd = 'dh-make-perl locate ' . join( ' ', @modules ) . ' 2>/dev/null';
    open my $fh, "-|", $cmd;
    my @packages;
    foreach (<$fh>) {
        # this ignores core packages
        # see DhMakePerl::Command::locate for details
        push @packages, $2 if m{(.+) is in (.+) package};
    }

    print join( ' ', sort @packages );
}

return 1;
